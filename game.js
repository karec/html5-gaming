/*
 global var of dialog
 inside: objects for dialog
 dialogObjects: contains 2 var => text(an array) and done(a boolean, default false)
*/


function initMenu() {
	var Qm = Quintus({ audioSupported: ['ogg', 'mp3']})
            		.include("Audio")
            		.setup({
                	width: 0,
                	height: 0,
            	}).enableSound();
            	Qm.load('menu.mp3', function() {
            		Qm.audio.play("menu.mp3", {loop:true});
            	});
            	
	document.getElementById("menuStart").addEventListener('click', function() {
		document.getElementById("menuBox").style.display = "none";
		Qm.audio.stop();
		Qm.clear();
		document.getElementById("quintus_container").remove();
		document.getElementById("progress").style.display = "block";
		init();
	});
	
	document.getElementById("menuInfos").addEventListener('click', function() {
		var menu = document.getElementById("menuBox");
		
		if (menu.style.backgroundImage == "url('images/bgmenuinfos.png')") {
			menu.style.background = "url('images/bgmenu.png')";
		} else {
			menu.style.background = "url('images/bgmenuinfos.png')";
		}

	});
}


var dialog = {
	intro: {
		text: ["Keros: where am I ?", 
		"dev: You're in kind of hell",
		"Keros: WHAT ?! Are you kinding me ?",
		"dev: No... And you're an evil ghost today, but you can change, you can get away",
		"Keros: ...How ?",
		"dev: You have to finish this level",
		"Keros: ...ok, let's do this"],
		done: false,
		lvl: 1
	},
	burgerDisco: {
		text: ["Keros: Wait a minut, this thing taste good !",
		"dev: This is a brainburger",
		"Keros: did you say brain ?",
		"dev: yes...",
		"Keros: oh god...And i have to eat this things ?",
		"dev: Well, if you want, it doesn't grant any bonus at all...",
		"Keros: oh god, you're so bad..."],
		done: false,
		lvl: 1
	},
	enemyDisco: {
		text: ["Keros: this things....",
		"dev: They are hostile, you have to kill them or they will kill you",
		"Keros: But how i kill this ?",
		"dev: There heads are just brain, so jump on it and that it",
		"Keros: are you kidding me ?...",
		"dev: No",
		"Keros: Are you fucking stupid ? I'm a ghost ! How can I kill them by jumping over there head ?",
		"dev: Shut'up and do it, I'm the god here !",
		"Keros: Well..."
		],
		done: false,
		lvl: 1
	},
	endDisco: {
		text: ["Keros: An orb ? rly ?! This is sooooo old man",
		"dev: This is your escape, juste walk in and you'll be teleported",
		"Keros: Yeah yeah i know... But AN ORB !! Man you realy need creativity...",
		"dev: SHUT UP AND WALK IN THIS ORB DAMN IT !"
		],
		done: false,
		lvl: 1
	},
	stubbIntro: {
		text: ["dev: Hi stubb, welcome to hell...", 
		"Stubb: brain ?",
		"dev: oh yeah I forgot, you only know this word right ?",
		"Stubb: brain...",
		"dev: Ok, listen, you just have to escape this cave, it's easy !"],
		done: false,
		lvl: 2
	},
	stubbBurger: {
		text: ["Stubb: BRAIIIN !!!!",
		"dev: oh god, I'v created a monster..."],
		done: false,
		lvl: 2
	},
	stubbEnnemies: {
		text: ["dev: care Stubb, this creature are your ennemies !",
		"Stubb: brain ?",
		"dev: no, they don't have brain actually..."],
		done: false,
		lvl: 2
	},
	bossDisco: {
		text: ["Keros: WHAT IS THIS THING",
		"dev: an ancient lich",
		"Keros: how i'm suppose to kill it",
		"dev: guess what...", 
		"Keros: ... i'm tired of this"],
		done: false,
		lvl: 3
	},
	bossWeaker: {
		text: [
			"dev: He's weaker, hold on !",
			"Keros: YEAH TAKE THAT"
		],
		done: false,
		lvl: 3
	},
	bossDead: {
		text: ["dev: YOU DID IT !",
		"Keros: yeah, that was easy...",
		"Keros: and now ?",
		"dev: Now it's time to continue your adventure !",
		"Keros: ..."],
		done: false,
		lvl: 3
	}
}


/* global var */
var currentLvl;

function makeDialog(dialog, Q, currentLvl) {
	if (dialog.done || dialog.lvl != currentLvl) {
		Q.unpauseGame();
		return true;
	}
		
	
	var currentIndex = 0;
	var dialBox = document.getElementById("DialBox");
	var button = document.getElementById("DialNext");
	var dialText = document.getElementById("DialText");
	var dialImage = document.getElementById("DialImage");
	
	dialText.innerHTML = dialog.text[currentIndex];
	if (dialog.text[currentIndex].indexOf('Keros') > -1) {
		dialImage.src = 'images/GrueGrin.png';
	} else if (dialog.text[currentIndex].indexOf('Stubb:') > -1) {
		dialImage.src = 'images/stubb.png';
	} else {
		dialImage.src = 'images/dev.png';
	}
	
	dialBox.style.display = "block";
	
	dialText.focus();
	
	button.addEventListener('click', function () {
		if (dialog.text[currentIndex + 1]) {
			currentIndex += 1;
			dialText.innerHTML = dialog.text[currentIndex];
			if (dialog.text[currentIndex].indexOf('Keros') > -1) {
				dialImage.src = 'images/GrueGrin.png';
			} 
			else if (dialog.text[currentIndex].indexOf('Stubb:') > -1) {
				dialImage.src = 'images/stubb.png';
			} else {
				dialImage.src = 'images/dev.png';
			}
		} else {
			dialog.done = true;
			dialBox.style.display = "none";
			Q.unpauseGame();
			this.removeEventListener('click',arguments.callee,false);
			document.getElementById("quintus").focus();
			if (dialog.text[0] == "dev: YOU DID IT !") {
				document.getElementById("endScreen").style.display = "none";	
			}
		}
	}, false);
	
	window.addEventListener('keydown', function (e) {
		if (e.keyCode == 13 || e.keyCode == 32) {
			if (dialog.text[currentIndex + 1]) {
				currentIndex += 1;
				dialText.innerHTML = dialog.text[currentIndex];
				if (dialog.text[currentIndex].indexOf('Keros') > -1) {
					dialImage.src = 'images/GrueGrin.png';
				} else if (dialog.text[currentIndex].indexOf('Stubb:') > -1) {
					dialImage.src = 'images/stubb.png'; 
				} else {
					dialImage.src = 'images/dev.png';
				}
			} else {
				dialog.done = true;
				dialBox.style.display = "none";
				Q.unpauseGame();
				this.removeEventListener('keydown',arguments.callee,false);
				document.getElementById("quintus").focus();
				if (dialog.text[0] == "dev: YOU DID IT !") {
					document.getElementById("endScreen").style.display = "block";	
				}
			}	
		}
	});
}

function init() {
	var Q = Quintus({ audioSupported: ['ogg', 'mp3']})
            .include("Sprites, Scenes, Input, 2D, Touch, UI, Anim, Audio")
            .setup({
                width: 960,
                height: 640,
                development: true
            }).controls().touch().enableSound();
      
        //player
        Q.Sprite.extend("Player",{
            init: function(p) {
                this._super(p, {sheet: "player", sprite:"player", x: 32*1, y: 278, jumpSpeed: -400, lives: 3, burgers: 0});
                this.add("2d, platformerControls");
                this.add("animation")
                this.p.timeInvincible = 0;

                this.on("hit.sprite",function(collision) {
                    if(collision.obj.isA("burger")) {
                    	Q.audio.play("miam.mp3");
                        collision.obj.destroy();
                        this.p.burgers++;
                        var burgersLabel = Q("UI.Text",1).items[1];
                        burgersLabel.p.label = 'burger x '+this.p.burgers;
                        
                    }
                });
                
                this.on("hit.sprite",function(collision) {
                	if(collision.obj.isA("EndLvl1")) {
        				Q.stageScene("endLvl",1, { label: "GG! c'est le debut de la fin"});
        				Q.audio.stop();
        				Q.audio.play("win.mp3");
        				this.destroy();
      				}
    			});
            },
            step: function(dt) {
                if(Q.inputs["left"] && this.p.direction == "right") {
                    this.p.flip = "x";
                } 
                if(Q.inputs["right"]  && this.p.direction == "left") {
                    this.p.flip = false;                    
                }
                
                if(this.p.y > 600) {
                	this.destroy();
                	 Q.audio.stop();
                	 Q.audio.play("dead.mp3", {loop:true});
     				 Q.stageScene("endGame",1, { label: "Vous etes mort!(pour de bon)" });
    			}
    			
    			if (Math.round(this.p.x) -2 <= (2*32) && Math.round(this.p.x) + 2 >= (2*32)) {
    				if (!dialog.intro.done) {
    					Q.pauseGame();
    					makeDialog(dialog.intro, Q, currentLvl);
    				}
    				if (!dialog.bossDisco.done && currentLvl == 3) {
    					Q.pauseGame();
    					makeDialog(dialog.bossDisco, Q, currentLvl);
    				}
    			}
    			
    			if ((Math.round(this.p.x) -2 <= (8*32 - 5) && Math.round(this.p.x) + 2 >= (8*32 - 5))) {
    				if (!dialog.burgerDisco.done) {
    					Q.pauseGame();
    					makeDialog(dialog.burgerDisco, Q, currentLvl);
    				}
    			}
    			
    			if ((Math.round(this.p.x) -2 <= (18*32 - 30) && Math.round(this.p.x) + 2 >= (18*32 - 30))) {
    				if (!dialog.enemyDisco.done) {
    					Q.pauseGame();
    					makeDialog(dialog.enemyDisco, Q, currentLvl);
    				}
    			}
    			
    			if ((Math.round(this.p.x) -2 <= (66*32) - 12 && Math.round(this.p.x) + 2 >= (66*32) - 12)) {
    				if (!dialog.endDisco.done) {
    					Q.pauseGame();
    					makeDialog(dialog.endDisco, Q, currentLvl);
    				}
    			}
                
                if(this.p.timeInvincible > 0) {
                    this.p.timeInvincible = Math.max(this.p.timeInvincible - dt, 0);
                }
            },
            damage: function() {
                //only damage if not in "invincible" mode, otherwise beign next to an enemy takes all the lives inmediatly
                if(!this.p.timeInvincible) {
                    this.p.lives--;
                    
                    this.p.timeInvincible = 1;
                    
                    if(this.p.lives<0) {
                    	Q.audio.stop();
                    	Q.audio.play("dead.mp3", {loop:true});
                        this.destroy();
                        Q.stageScene("endGame",1, { label: "Game Over" }); 
                    }
                    else {
                        Q.audio.play("zombie-3.mp3");
                        this.play("take_damage");
                        var livesLabel = Q("UI.Text",1).first();
                        livesLabel.p.label = "Lives x "+this.p.lives;
                    }
                }
            }
          });
        
         Q.Sprite.extend("PlayerZomb",{
            init: function(p) {
                this._super(p, {sheet: "zombies", sprite:"zombies", x: 64, y: 480, jumpSpeed: -400, lives: 3, burgers: 0});
                this.add("2d, platformerControls");
                this.add("animation")
                this.p.timeInvincible = 0;

                this.on("hit.sprite",function(collision) {
                    if(collision.obj.isA("burger")) {
                    	Q.audio.play("miam.mp3");
                        collision.obj.destroy();
                        this.p.burgers++;
                        var burgersLabel = Q("UI.Text",1).items[1];
                        burgersLabel.p.label = 'burger x '+this.p.burgers;
                        
                    }
                });
                
                this.on("hit.sprite",function(collision) {
                	if(collision.obj.isA("EndLvl1")) {
        				Q.stageScene("endLvl",1, { label: "GG! c'est le debut de la fin"});
        				Q.audio.stop();
        				Q.audio.play("win.mp3");
        				this.destroy();
      				}
    			});
            },
            step: function(dt) {
                
                if (this.p.vx > 0) {
                	if(!this.p.timeInvincible)
	                	this.play("run_right");
                } else if (this.p.vx < 0) {
                	if(!this.p.timeInvincible)
    	            	this.play("run_left");
                } else {
                	if(!this.p.timeInvincible)
                		this.play("stand_right");
                }
                
                if(this.p.y > 700) {
                	this.destroy();
                	 Q.audio.stop();
                	 Q.audio.play("dead.mp3", {loop:true});
     				 Q.stageScene("endGame",1, { label: "Vous etes mort!(pour de bon)" });
    			}
    			
    			if (Math.round(this.p.x) -2 <= (2*32) && Math.round(this.p.x) + 2 >= (2*32)) {
    				if (!dialog.stubbIntro.done) {
    					Q.pauseGame();
    					makeDialog(dialog.stubbIntro, Q, currentLvl);
    				}
    			}
    			
    			if ((Math.round(this.p.x) -2 <= (17*32 - 15) && Math.round(this.p.x) + 2 >= (17*32 - 15))) {
    				if (!dialog.stubbBurger.done) {
    					Q.pauseGame();
    					makeDialog(dialog.stubbBurger, Q, currentLvl);
    				}
    			}
    			
    			if ((Math.round(this.p.x) -2 <= (22*32 - 10) && Math.round(this.p.x) + 2 >= (22*32 - 10))) {
    				if (!dialog.stubbEnnemies.done) {
    					Q.pauseGame();
    					makeDialog(dialog.stubbEnnemies, Q, currentLvl);
    				}
    			}
                
                if(this.p.timeInvincible > 0) {
                    this.p.timeInvincible = Math.max(this.p.timeInvincible - dt, 0);
                }
            },
            damage: function() {
                //only damage if not in "invincible" mode, otherwise beign next to an enemy takes all the lives inmediatly
                if(!this.p.timeInvincible) {
                    this.p.lives--;
                    
                    this.p.timeInvincible = 1;
                    
                    if(this.p.lives<0) {
                    	Q.audio.stop();
                    	Q.audio.play("dead.mp3", {loop:true});
                        this.destroy();
                        Q.stageScene("endGame",1, { label: "Game Over" }); 
                    }
                    else {
                        Q.audio.play("zombie-3.mp3");
                        this.play("take_damage");
                        var livesLabel = Q("UI.Text",1).first();
                        livesLabel.p.label = "Lives x "+this.p.lives;
                    }
                }
            }
          });
        
        //component for common enemy behaviors
        Q.component("commonEnemy", {
            added: function() {
                var entity = this.entity;
                entity.on("bump.left,bump.right,bump.bottom",function(collision) {
                    if(collision.obj.isA("Player") || collision.obj.isA("PlayerZomb")) {                        
                      collision.obj.damage();
                    }
                });
                entity.on("bump.top",function(collision) {
                    if(collision.obj.isA("Player") || collision.obj.isA("PlayerZomb")) { 
                        collision.obj.p.vy = -100;
                        this.destroy();
                    }
                });
            },
            
        });

		//common boss component

        Q.Sprite.extend("GroundEnemy", {
            init: function(p) {
            	this.life = 3;
                this._super(p, {vx: -100, sprite:"brain"});
                this.add("animation, 2d, aiBounce, commonEnemy");
            },
            
            step: function(dt) {
                var dirX = this.p.vx/Math.abs(this.p.vx);
                var ground = Q.stage().locate(this.p.x, this.p.y + this.p.h/2 + 1, Q.SPRITE_DEFAULT);
                var nextTile = Q.stage().locate(this.p.x + dirX * this.p.w/2 + dirX, this.p.y + this.p.h/2 + 1, Q.SPRITE_DEFAULT);
                
                if (this.p.vx > 0) {
                	this.play("run_right");
                } else if (this.p.vx < 0) {
                	this.play("run_left");
                } else {
                	this.play("stand_left");
                }
                
                
                //if we are on ground and there is a cliff
                if(!nextTile && ground) {
                    if(this.p.vx > 0) {
                        if(this.p.defaultDirection == "right") {
							this.play("run_right");                            
                        }
                    }
                    else {
                        if(this.p.defaultDirection == "left") {
                            this.play("run_left");
                        }
                    }
                    this.p.vx = -this.p.vx;
                }
            }
        	
        });
        
         Q.Sprite.extend("Boss", {
            init: function(p) {
                this._super(p, {vx: -100, sprite:"boss", sheet:"boss", defaultDirection: "right"});
                this.add("animation, 2d, aiBounce");
                this.p.flip = "x";
                var life = 12;
                //var entity = this.entity;
                this.on("bump.left,bump.right,bump.bottom",function(collision) {
                    if(collision.obj.isA("Player") || collision.obj.isA("PlayerZomb")) {                        
                      collision.obj.damage();
                    }
                });
                this.on("bump.top",function(collision) {
                    if(collision.obj.isA("Player") || collision.obj.isA("PlayerZomb")) {
                    	if (life > 6) {
                    		this.play("take_damage");
                    	} else {
                    		this.play("damage_phase2");
                    	}
                    	collision.obj.p.vy = -400;	 
                        life -= 1;
                        if (life == 6) {
                        	Q.pauseGame();
                        	makeDialog(dialog.bossWeaker, Q, 3);
                        	this.play("phase2");
                        	collision.obj.p.x = 32*2;
                        	collision.obj.p.y = 13*32;
                        }
                        else if (life == 0) {
                        	this.destroy();
                        	Q.pauseGame();
                        	makeDialog(dialog.bossDead, Q, 3);
                        	Q.audio.stop();
                        	Q.audio.play("win.mp3");
                        	Q.audio.play("menu.mp3", { loop: true });
                        	Q.clearStages();
                        }
                    }
                });
            },
            
            step: function(dt) {
                var dirX = this.p.vx/Math.abs(this.p.vx);
                var ground = Q.stage().locate(this.p.x, this.p.y + this.p.h/2 + 1, Q.SPRITE_DEFAULT);
                var nextTile = Q.stage().locate(this.p.x + dirX * this.p.w/2 + dirX, this.p.y + this.p.h/2 + 1, Q.SPRITE_DEFAULT);
                
              	if(!nextTile && ground) {
     			   if(this.p.vx > 0) {
            	   		if(this.p.defaultDirection == "right") {
	                		this.p.flip = "x";
            			} else {
	                		this.p.flip = false;
            			}
        			} else {
            			if(this.p.defaultDirection == "left") {
                			this.p.flip = "x";
            			} else {
                			this.p.flip = false;
            			}
        			}
        			this.p.vx = -this.p.vx;
    			}  
             }
        });
        
        Q.Sprite.extend("burger", {
            init: function(p) {
                this._super(p, {asset: "burger.png"});
            }            
        });
        
        Q.scene("level1",function(stage) {
          
          	currentLvl = 1;
            var background = new Q.TileLayer({ dataAsset: "discover.tmx", layerIndex: 0, sheet: "tiles", tileW: 32, tileH: 32, type: Q.SPRITE_NONE });
            var bg2 = new Q.TileLayer({ dataAsset: "discover.tmx", layerIndex: 1, sheet: "tiles", tileW: 32, tileH: 32, type: Q.SPRITE_NONE });
            stage.insert(background);
            stage.insert(bg2);
            
            stage.collisionLayer(new Q.TileLayer({ dataAsset: "discover.tmx", layerIndex:2,  sheet: "tiles", tileW: 32, tileH: 32, type: Q.SPRITE_DEFAULT }));
            
            var player = stage.insert(new Q.Player());
            
            var levelAssets = [
            	["GroundEnemy", {x: 53*32, y: 3*32, sheet:"brainMonster"}], //little top bloc
                ["GroundEnemy", {x: 37*32, y: 3*32, sheet:"brainMonster"}], //little top bloc
                ["GroundEnemy", {x: 37*32, y: 15*32, sheet:"brainMonster"}], //2nd big bloc
                ["GroundEnemy", {x: 30*32, y: 11*32, sheet:"brainMonster"}], //big 1st bloc
                ["GroundEnemy", {x: 26*32, y: 11*32, sheet:"brainMonster"}], //big 1st bloc
                ["burger", {x: 2001, y: 215}],
                ["burger", {x: 1841, y: 150}],
                ["burger", {x: 1553, y: 119}],
                ["burger", {x: 1393, y: 151}],
                ["burger", {x: 1329, y: 215}],
                ["burger", {x: 1105, y: 215}],
                ["burger", {x: 1008, y: 116 }],
                ["burger", {x: 590, y: 150}],
                ["burger", {x: 560, y: 438}],
                ["burger", {x: 400, y: 566}],
                ["burger", {x: 400, y: 342}],
                ["burger", {x: 240, y: 278}],
                ["burger", {x: 240, y: 438}]
                /* -16 x -10 y*/
            ];
            
            //load level assets
            stage.loadAssets(levelAssets);  
            stage.insert(new Q.EndLvl1({x: 2144, y: 360}));
            stage.insert(new Q.GroundEnemy({x:23*32, y:6*32}));
            stage.add("viewport").follow(player,{x: true, y: true},{minX: 0, maxX: background.p.w, minY: 0, maxY: background.p.h});
          
        });
        
        Q.scene('level2', function(stage) {
        	
        	currentLvl = 2;
        	var bg2 = new Q.TileLayer({ dataAsset: "level2.tmx", layerIndex: 1, sheet: "tiles", tileW: 32, tileH: 32, type: Q.SPRITE_NONE });
        	
        	stage.insert(new Q.Repeater({ asset: "level2.png", speedX: 0.5, speedY: 0.5 }));
        	stage.insert(bg2);
        	
        	stage.collisionLayer(new Q.TileLayer({ dataAsset: "level2.tmx", layerIndex:2,  sheet: "tiles", tileW: 32, tileH: 32, type: Q.SPRITE_DEFAULT }));
        	
        	var player_lvl2 = stage.insert(new Q.PlayerZomb());
        	
        	var levelAssets = [
            	
                ["GroundEnemy", {x: 33*32, y: 13*32, sheet:"skull"}],
                ["GroundEnemy", {x: 28*32, y: 13*32, sheet:"skull"}],
                ["GroundEnemy", {x: 10*32, y: 5*32, sheet:"skull"}],
                ["GroundEnemy", {x: 17*32, y: 6*32, sheet:"skull"}],
                ["GroundEnemy", {x: 45*32, y: 17*32, sheet:"skull"}],
                ["GroundEnemy", {x: 47*32, y: 7*32, sheet:"skull"}],
                ["GroundEnemy", {x: 54*32, y: 8*32, sheet:"skull"}],
                ["burger", {x: 17*32 - 15, y: 16*32 - 16}],
                ["burger", {x: 22*32 - 15, y: 14*32 - 16}],
                ["burger", {x: 24*32 - 15, y: 8*32 - 16}],
                ["burger", {x: 1*32 - 15, y: 4*32 - 16}],
                ["burger", {x: 6*32 - 15, y: 5*32 - 16}],
                ["burger", {x: 37*32 - 15, y: 12*32 - 16}],
                ["burger", {x: 38*32 - 15, y: 6*32 - 16}],
                ["burger", {x: 42*32 - 15, y: 7*32 - 16}],
                ["burger", {x: 53*32 - 15, y: 16*32 - 16}]
            ];

			
			//load level assets
            stage.loadAssets(levelAssets); 
			
            stage.insert(new Q.EndLvl1({x: 26*32, y: 10}));
            stage.insert(new Q.GroundEnemy({x:23*32, y:6*32}));
            stage.add("viewport").follow(player_lvl2);
        	
        });
        
       Q.scene('level3', function(stage) {
        	
        	currentLvl = 3;
        	var bg1 = new Q.TileLayer({ dataAsset: "boss.tmx", layerIndex: 0, sheet: "tiles", tileW: 32, tileH: 32, type: Q.SPRITE_NONE });
        	var bg2 = new Q.TileLayer({ dataAsset: "boss.tmx", layerIndex: 1, sheet: "tiles", tileW: 32, tileH: 32, type: Q.SPRITE_NONE });
        	
        	stage.insert(bg1);
        	stage.insert(bg2);
        	
        	stage.insert(new Q.Boss({x: 16*32, y: 10*32 + 50}));
        	
        	stage.collisionLayer(new Q.TileLayer({ dataAsset: "boss.tmx", layerIndex:2,  sheet: "tiles", tileW: 32, tileH: 32, type: Q.SPRITE_DEFAULT }));
        	
        	var player = stage.insert(new Q.Player());


            stage.insert(new Q.GroundEnemy({x:23*32, y:6*32}));
            stage.add("viewport").follow(player,{x: true, y: true},{minX: 0, maxX: bg1.p.w, minY: 0, maxY: bg1.p.h});
        	
        });
        
        Q.Sprite.extend("EndLvl1", {
  				init: function(p) {
    			this._super(p, { asset: 'end.png' });
  			}
		});
        
        Q.scene("endGame",function(stage) {
        	
        	var box = stage.insert(new Q.UI.Container({
        		x: Q.width/2, y: Q.height/2
        	}));
        	
        	var button = box.insert(new Q.UI.Button({ x: 0, y: 0, fill: "#FFFFFF", label: "Rejouer"}));
        	
        	var label = box.insert(new Q.UI.Text({ x:10, y:-10 - button.p.h, label: stage.options.label}));
        	
        	button.on("click", function() {
        		Q.audio.stop();
        		Q.clearStages();
        		Q.stageScene("level" + currentLvl);
        		Q.stageScene("gameStats",1);
        		Q.audio.play("boss.mp3", {loop:true});
        	});
        	
        	box.fit(20);
        });
        
        Q.scene("endLvl", function(stage) {
        	
        	var box = stage.insert(new Q.UI.Container({
        		x: Q.width/2, y: Q.height/2
        	}));
        	
        	var button = box.insert(new Q.UI.Button({ x: 0, y: 0, fill: "#FFFFFF", label: "Niveau suivant"}));
        	
        	var label = box.insert(new Q.UI.Text({ x:10, y:-10 - button.p.h, label: "Niveau fini !"}));
        	
        	button.on("click", function() {
        		Q.audio.stop();
        		Q.clearStages();
        		Q.stageScene("level" + (currentLvl + 1));
        		Q.stageScene("gameStats",1);
        		if (currentLvl == 3) {
        			Q.audio.play("boss.mp3", {loop:true});
        		} else {
        			Q.audio.play("boss.mp3", {loop:true});	
        		}
        	});
        	
        	box.fit(20);
        	
        });

        Q.scene("gameStats", function(stage) {
            var statsContainer = stage.insert(new Q.UI.Container({
                fill: "transparent",
                x: 960/2,
                y: 20,
                border: 0,
				shadow: 3,
                shadowColor: "",
                w: 960,
                h: 40
                })
            );
                
            var lives = stage.insert(new Q.UI.Text({ 
                    label: "Lives x 3",
                    color: "white",
                    x: -300,
                    y: 0
                }),statsContainer);
            
            var burgers = stage.insert(new Q.UI.Text({ 
                    label: "burgers x 0",
                    color: "white",
                    x: 300,
                    y: 0
                }),statsContainer);
                
             
        });
        
        //load assets
        Q.load("tiles_cim.png, level2.png, global_sprites.png, discover.tmx, level2.tmx, boss.tmx, burger.png, end.png, global_tiles.png ,sprites.json, dead.mp3, menu.mp3, zombie-3.mp3, miam.mp3, boss.mp3, win.mp3", function() {            
            Q.sheet("tiles","global_tiles.png", { tilew: 32, tileh: 32});
            Q.compileSheets("global_sprites.png", "sprites.json");
            
            Q.animations('zombies', {
      			run_right: { frames: [6,7,8], rate:1/8 },
      			run_left: {frames: [3,4,5], rate: 1/8},
      			stand_right: {frames: [1,2,3]},
      			stand_left: {frames: [10,11,12]},
      			take_damage: {frames: [10,1,10,1,10,1,10,1,10,1,10], rate: 1/8, loop:false}
      		});
      		
      		Q.animations('brain', {
      			run_right: { frames: [6,7,8], rate:1/6 },
      			run_left: {frames: [3,4,5], rate: 1/6},
      			stand_right: {frames: [9,10,11], rate:1/6},
      			stand_left: {frames: [9,10,11], rate:1/6}
      		});
      		
      		Q.animations('player1', {
      			run_right: { frames: [18,19,20], rate:1/8}, 
  				run_left: { frames: [9,10,11], rate:1/8},
  				stand_right: {frames: [1,2,3] },
      			stand_left: {frames: [1,2,3] }
      		});
      		
      		Q.animations('player', {
      			take_damage: { frames: [0,1,0,1,0,1,0,1,0,1,0], rate: 1/8, loop:false}
      		});
            
            Q.animations("boss", {
            	take_damage: { frames: [0,1,0,1,0,1,0,1,0,1,0,1,0], rate: 1/8, loop:false},
            	phase2: {frames: [2]},
            	damage_phase2: {frames: [2,1,2,1,2,1,2,1,2,1,2,1,2], rate: 1/8, loop:false}
            });
            
            Q.stageScene("level1");
            Q.stageScene("gameStats",1);
            Q.audio.play("boss.mp3", {loop:true} );
        }, {progressCallback: function(e) {
        	var pourcent = 100 / 16; 
        	var progress = document.getElementById("progressBar");
        	
        	progress.style.width = (e * pourcent) + "%";
        	
        	if (progress.style.width == "100%") {
        		document.getElementById("progress").style.display = "none";
        	}
        	
        }});
}
